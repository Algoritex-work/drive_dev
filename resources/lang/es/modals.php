<?php

return [
    'new_folder' => 'Crear Nueva Carpeta',
    'cancel' => 'Cancelar',
    'folder_name' => 'Nombre: ',
    'create_folder' => 'Cerar Carpeta: ',

    'new_upload' => 'Subir',
    'file' => ' Archivo ',
    'upload' => 'Subir Archivo',

    'preview_image' => 'Previsualizar Imagen',

    'new_user_data' => 'Mis datos de usuario: ',
    'dni' => 'Dni: ',
    'email' => 'Email: ',
    'name' => 'Nombre: ',
    'username' => 'Usuario: ',
    'register' => 'Registro: ',
    'turn' => 'Turno: ',
    'phone' => 'Telef&oacute;no: ',
    'role' => 'Role: ',

];

