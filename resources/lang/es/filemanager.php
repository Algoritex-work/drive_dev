<?php

return [
     'th_full_path' => 'FULL PATH',
     'th_name' => 'Nombre',
     'th_mime' => 'Tipo',
     'th_size' => 'Tama&ntilde;o',
     'th_date' => 'Fecha',
     'th_prio' => 'Prio',
     'th_actions' => 'Acciones',

];