<?php

return [
    'title_login' => 'OepbCloud Estibador@s Barcelona',
    'email' => ' Email',
    'password' => 'Contrase&ntilde;a',
    'remenber_me' => 'Recordar',
    'enter' => 'Entrar',
    'resore_pass' => 'Reg&iacute;strate o restaura tu contrase&ntilde;a aqu&iacute;',
    'err_credentials' => 'Las credenciales no son correctas',
];