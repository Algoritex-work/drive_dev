<?php

return [
    'title' => 'OepbCloud Estibador@s Barcelona',
    'btn_search' => 'B&uacute;squeda',
    'role_lbl' => 'role: ',
    'admin_role' => ' Administrar roles',
    'drive' => ' Drive ',
    'details' => ' Ver mis datos',
    'close_session' => ' Cerrar Sessi&oacute;n',
];