<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Credenciales no v&aacute;lidas.',
    'throttle' => 'Demsiados intentos fallidos, por favor int&eacute;ntalo pasados unos segundos',


];


