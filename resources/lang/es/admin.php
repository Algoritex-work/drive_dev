<?php

return [
    'email' => 'Email',
    'dni' => 'Dni',
    'username' => 'Usuario',
    'name' => 'Nombre',
    'role' => 'Rol',
    'new_role' => 'Nuevo Rol',

    'estibador' => 'estibador',
    'admin' => 'admin',
    'normal' => 'normal',
    'btn_change' => 'Cambiar',
];