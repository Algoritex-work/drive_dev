@extends('master')


@section('content')

    @include('breadcrum')

    <br>

    @include('filemanager')
    @include('modals.modals')

@stop