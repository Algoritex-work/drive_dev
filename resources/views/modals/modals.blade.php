
<!--
  MODAL CREATE FOLDER
-->
<div class="modal fade" id="modal-folder-create">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="filesystem.folder"
                  class="form-horizontal">
                {!! csrf_field() !!}
                <input type="hidden" name="current_path" value="{{ $data['full_path']}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                    <h4 class="modal-title"> {{ trans('modals.new_folder') }} </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="new_folder_name" class="col-sm-3 control-label">
                             {{ trans('modals.folder_name') }}
                        </label>
                        <div class="col-sm-8">
                            <input type="text" id="new_folder_name" name="new_folder"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                         {{ trans('modals.cancel') }}
                    </button>
                    <button type="submit" class="btn btn-primary">
                         {{ trans('modals.new_folder') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>



<!--
  MODAL FILE UPLOAD
-->
<div class="modal fade" id="modal-file-upload">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="form_upload" action="/filesystem.upload"
                  class="form-horizontal" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="current_path" value="{{ $data['full_path'] }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                    <h4 class="modal-title">{{ trans('modals.new_upload') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="file" class="col-sm-3 control-label">
                            {{ trans('modals.file') }}
                        </label>
                        <div class="col-sm-8">
                            <input type="file" id="file" name="file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('modals.cancel') }}
                    </button>
                    <button type="submit" class="btn btn-primary">
                       {{ trans('modals.upload') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- View Image Modal --}}
<div class="modal fade" id="modal-image-view">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
        <i class="glyphicon glyphicon-remove"></i>
        </button>
        <h4 class="modal-title">{{ trans('modals.preview_image') }}</h4>
      </div>
      <div class="modal-body">
       <img id="preview-image" src="" class="img-responsive">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>




