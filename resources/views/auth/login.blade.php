
@extends('auth.common_auth_refs')

@section('auth_content')

    <div class="row align-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 login_wrapper">
            <!-- Login Block -->
            <div class="block block-themed animated fadeIn">
                <div class="block-content block-content-full block-content-narrow">
                    <!-- Login Title -->
                    <h1 class="h2 font-w600 push-30-t push-5 text-center">{{  trans('login.title_login') }}</h1>
                    <hr>
                    <!-- Login Form -->
                    <form class="js-validation-login form-horizontal push-30-t push-50" action="/auth/login"
                          method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                                    <label for="login-username">{{  trans('login.email') }}</label>
                                    <input class="form-control" type="text" id="login-username" name="email">

                        </div>

                        <div class="form-group">
                                <div class="form-material form-material-primary floating">

                                    <label for="login-password">{{  trans('login.password') }}</label>
                                    <input class="form-control" type="password" id="login-password"
                                           name="password">
                                </div>
                        </div>

                        <div class="form-group">
                                <label class="css-input switch switch-sm switch-primary">
                                    <input type="checkbox" id="login-remember-me" name="remember"><span></span>
                                    {{  trans('login.remenber_me') }}
                                </label>
                        </div>

                        <div class="form-group text-center">
                                <button class="btn btn-block btn-primary"
                                        type="submit">{{  trans('login.enter') }}</button>
                                <!--
                                <a href="/auth/register" class="btn btn-block btn-danger">Registro</a>
                                -->

                        </div>
                        <div class="form-group text-center">
                            <a href="http://oepb.org/app/oepbapp/" >
                                {{  trans('login.resore_pass') }}
                            </a>

                        </div>

                        @if (count($errors) > 0)
                            <div class="alert-danger text-center">
                                <br>
                                {{  trans('login.err_credentials') }}
                                <br>
                                <br>
                            </div>
                        @endif

                    </form>
                    <!-- END Login Form -->
                </div>
            </div>
            <!-- END Login Block -->
        </div>
    </div>
@stop