@extends('auth.common_auth_refs')

@section('auth_content')

        <!-- Register Content -->
<div class="content overflow-hidden">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <div class="block-content block-content-full block-content-narrow">
                <!-- Register Title -->
                <h1 class="h2 font-w600 push-30-t push-5">Registro</h1>
                <!-- END Register Title -->

                <!-- Register Form -->
                <!-- jQuery Validation (.js-validation-register class is initialized in js/pages/base_pages_register.js) -->
                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                <form class="js-validation-register form-horizontal push-50-t push-50" action="/auth/register"
                      method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="register-email">Dni</label>
                                <input class="form-control" id="register-dni" name="dni" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="register-email">Email</label>
                                <input class="form-control" type="email" id="register-email" name="email"
                                       placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="register-password">Password</label>
                                <input class="form-control" type="password" id="register-password" name="password"
                                       placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="register-password2">Confirm Password</label>
                                <input class="form-control" type="password" id="register-password2"
                                       name="password_confirmation" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="email">Registro </label>
                                <input type="text" id="register" name="register" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="turn">Turno </label>
                            <input type="text" id="turn" name="turn" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="full_name">Usuario</label>
                            <input type="text" id="username" name="username" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="full_name">Nombre Completo</label>
                            <input type="text" id="full_name" name="full_name" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material form-material-success">
                                <label for="phone">Tel&eacute;fono </label>
                                <input type="text" id="phone" name="phone" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 col-md-5">
                            <button class="btn btn-block btn-success" type="submit"><i
                                        class="fa fa-plus pull-right"></i> Enviar
                            </button>
                        </div>
                    </div>
                </form>
                <!-- END Register Form -->
            </div>
        </div>
        <!-- END Register Block -->
    </div>
</div>
</div>
<!-- END Register Content -->


@stop