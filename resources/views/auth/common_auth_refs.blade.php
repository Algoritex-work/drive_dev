<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
        <meta name="csrf-token" content="csrf_token()" />
        -->
    <title>Drive OEPB</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>

    <link rel="stylesheet" type="text/css" href="/css/select.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/my_css.css"/>


</head>
<body class="login_body">
<!--//BLOQUE COOKIES-->
<div style="display: block;" id="barraaceptacion">
    <div class="inner">
        <br>
        Solicitamos su permiso para obtener datos estad&iacutesticos de su navegaci&oacute;n en esta web, en cumplimiento del Real
        Decreto-ley 13/2012. Si contin&uacute;a navegando consideramos que acepta el uso de cookies.
        <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> |
        <a href="http://politicadecookies.com" target="_blank" class="info">M&aacute;s informaci&oacute;n</a>
    </div>
</div>
<div class="container">
    @yield('auth_content')
</div>

<div class="container">
    <div class="row">

    </div>
</div>





<script>
    function getCookie(c_name){
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1){
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1){
            c_value = null;
        }else{
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1){
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    }

    function setCookie(c_name,value,exdays){
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    }

    if(getCookie('tiendaaviso')!="1"){
        document.getElementById("barraaceptacion").style.display="block";
    }
    function PonerCookie(){
        setCookie('tiendaaviso','1',365);
        document.getElementById("barraaceptacion").style.display="none";
    }
</script>
<!--//FIN BLOQUE COOKIES-->


<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
</body>
</html>