<div class="header">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-center text-center">
            <div id="logo">
                <a href="/">
                    <h3>
                        {{ trans('header.title') }}
                    </h3>
                </a>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-center text-center margin_search">
            <form method="post" action="filesystem.search">
                {!! csrf_field() !!}
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="" name="search_query">
                 <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">{{ trans('header.btn_search') }}</button>
          </span>
                </div>
            </form>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-center text-center margin_info">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="glyphicon glyphicon-user"></i>  {!! $user->username !!}
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    @if ($user->role == "admin")
                        <li><a href="/admin">  <span class="glyphicon glyphicon-cog"></span>{{ trans('header.admin_role') }}</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/">  <span class="glyphicon glyphicon-hdd"></span>{{ trans('header.drive') }} </a></li>
                    @endif
                    <li role="separator" class="divider"></li>
                    <li><a href="#" data-toggle="modal" data-target="#modal_user_details">  <span class="glyphicon glyphicon-pencil"></span>{{ trans('header.details') }}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/auth/logout"> <span class="glyphicon glyphicon-log-out"></span> {{ trans('header.close_session') }}</a></li>
                </ul>
            </div>

            <!-- Split button -->
            <div class="btn-group">
                <i class="glyphicon glyphicon-star-empty"></i>  {{ trans('header.role_lbl') }} {{ $user->role}}
            </div>

        </div>

    </div>
</div>