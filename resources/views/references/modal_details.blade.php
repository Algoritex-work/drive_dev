
            <!-- Modal User Details
       =============================================
    -->
    <div class="modal fade" id="modal_user_details" tabindex="-1" role="dialog">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal_form_recovery_label">{{ trans('modals.new_user_data') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="dni">{{ trans('modals.dni') }}</label>
                        {!! $user->dni !!}
                    </div>
                    <div class="form-group">
                        <label for="email">{{ trans('modals.email') }} </label>

                        {!! $user->email !!}
                    </div>
                    <div class="form-group">
                        <label for="full_name">{{ trans('modals.name') }}</label>
                        {!! $user->full_name !!}
                    </div>

                    <div class="form-group">
                        <label for="username">{{ trans('modals.username') }}</label>
                        {!! $user->username !!}
                    </div>
                    <div class="form-group">
                        <label for="register">{{ trans('modals.register') }} </label>
                        {!! $user->register !!}
                    </div>

                    <div class="form-group">
                        <label for="turn">{{ trans('modals.turn') }}</label>
                        {!! $user->turn !!}
                    </div>

                    <div class="form-group">
                        <label for="phone">{{ trans('modals.phone') }}</label>
                        {!! $user->phone !!}
                    </div>
                    <div class="form-group">
                        <label for="role">{{ trans('modals.role') }}</label>
                        {!! $user->role !!}
                    </div>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal User Details Form End -->

