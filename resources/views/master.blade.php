<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
        <meta name="csrf-token" content="csrf_token()" />
        -->
    <title>Drive OEPB</title>


    <!--
    <link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css"/>
    -->
    <link rel="stylesheet" type="text/css" href="/css/responsive.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css"/>

    <link rel="stylesheet" type="text/css" href="/css/select.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/select.dataTables.min.css"/>
    <!--
    <link rel="stylesheet" type="text/css" href="/css/datatables.css"/>
    -->
    <link rel="stylesheet" type="text/css" href="/lightbox/css/lightbox.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/my_css.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link href="http://vjs.zencdn.net/5.8.0/video-js.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>

</head>

<body>


<div class="container" id="main_container">
    @include('references.header')
    <br>
    @yield('content')
    @include('references.modal_details')
    @include('references.footer')

</div>


@include('references.loader')


        <!--
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

</script>
-->


<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/dataTables.responsive.min.js"></script>
<!--
<script type="text/javascript" src="/js/dataTables.select.min.js"></script>
-->
<script type="text/javascript" src="/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="/js/my_js.js"></script>
<script type="text/javascript" src="/js/table_users.js"></script>
<script type="text/javascript" src="/lightbox/js/lightbox.min.js"></script>



<!-- If you'd like to support IE8 -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.8.0/video.js"></script>

<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/ViewerJS/my_js.js"></script>
<script type="text/javascript" src="/js/table_files.js"></script>
-->


</body>
</html>








