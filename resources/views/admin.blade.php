@extends('master')


@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">

            @if (session('message'))
                <div class="alert-success text-center">
                    <br>
                    {{ session('message') }}
                    <br>
                    <br>
                </div>
            @endif
            @if (session('error'))
                <div class="alert-danger text-center">
                    <br>
                    {{ session('error') }}
                    <br>
                    <br>
                </div>
            @endif
            <br>
            <!-- Dynamic Table Full -->
            <div class="block">
                <div class="block-header">
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped" id='table_users' width="100%">
                        <thead>
                        <tr>
                            <th> {{  trans('admin.email') }} </th>
                            <th> {{  trans('admin.dni') }} </th>
                            <th> {{  trans('admin.username') }} </th>
                            <th> {{  trans('admin.name') }} </th>
                            <th> {{  trans('admin.role') }}  </th>
                            <th data-sortable="false"> {{  trans('admin.new_role') }} </th>
                        </tr>
                        </thead>
                        <tbody>

                        <!-- FOLDERS -->
                        @foreach ($data as $datauser)
                            <tr>
                                <td>{{ $datauser->email }}</td>
                                <td>{{ $datauser->dni }}</td>
                                <td>{{ $datauser->username }}</td>
                                <td>{{ $datauser->full_name }}</td>
                                <td>{{ $datauser->role }}</td>
                                <td>
                                    <form role="form" class="form_update_role" method="post" action="admin.update_role">
                                        <div class="form-group">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="email_user" id="email_user"
                                                 value="{{ $datauser->email }}"/>
                                            <div class="form-group">
                                                <div class="radio">
                                                    <label><input type="radio" name="role" value="normal">Normal</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" name="role" value="admin">Administrador</label>
                                                </div>
                                                <div class="radio disabled">
                                                    <label><input type="radio" name="role" value="estib">Estibador</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-info" type="submit">
                                                    {{  trans('admin.btn_change') }}
                                            <!--
                                                <select class="form-control" id="role" name="role">
                                                    <option>  trans('admin.normal') }}  </option>
                                                    <option>  trans('admin.admin') }}  </option>
                                                    <option>  trans('admin.estibador') }}  </option>
                                                </select>
                                                -->
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Dynamic Table Full -->

        </div>
    </div>



@stop




