<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
        <meta name="csrf-token" content="csrf_token()" />
        -->
    <title>Drive OEPB</title>

    <link href="http://vjs.zencdn.net/5.8.0/video-js.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>

</head>

<body>

<div class="container">
    <div class="row align-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            @if ( isset ($link) && isset($type))
                @if ($type == "video")
                <video id="my-video" class="video-js" controls preload="auto" width="300px" height="200px" data-setup="{}">
                    <source src="{{$link}}" type='video/mp4'>
                    <source src="{{$link}}" type='video/webm'>
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
                @else
                    <audio src="{{$link}}" preload="auto" />
                @endif
            @endif
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="/js/my_js.js"></script>

<!-- If you'd like to support IE8 -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.8.0/video.js"></script>

<script src="/audiojs/audio.min.js"></script>


<script>
    audiojs.events.ready(function() {
        var as = audiojs.createAll();
    });
</script>


</body>
</html>








