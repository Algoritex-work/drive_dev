
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-center text-center">
        <div class="pull-left">
            <ul class="breadcrumb" id="route_path">
                @foreach ($data['breadcrum'] as $crum)
                    <li>
                        @if ( $crum['name'] == "ROOT")
                            <a href="filesystem.create?path={{ $crum['full_path'] }}" class='breadcrum_link'> <img src="/images/hd-50.png"/></a>
                        @else
                            <a href="filesystem.create?path={{ $crum['full_path'] }}" class='breadcrum_link'> {{ $crum['name'] }}</a>
                        @endif
                    </li>

                @endforeach
            </ul>
        </div>
    </div>
    @if ($user->role == "admin")
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-center text-center">
            <button type="button" class="btn btn-success btn-md"
                    data-toggle="modal" data-target="#modal-folder-create">
                <i class="glyphicon glyphicon-plus-sign"></i>  {{  trans('breadcrum.folder') }}
            </button>
            <button type="button" class="btn btn-primary btn-md"
                    data-toggle="modal" data-target="#modal-file-upload">
                <i class="glyphicon glyphicon-cloud-upload"></i>  {{  trans('breadcrum.upload') }}
            </button>
        </div>
    @endif

</div>
