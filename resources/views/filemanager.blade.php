<!--
DATATABLES
-->

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if (session('message'))
            <div class="alert-success text-center">
                <br>
                {{ session('message') }}
                <br>
                <br>
            </div>
            <br>
        @endif
        @if (session('error'))
            <div class="alert-danger text-center">
                <br>
                {{ session('error') }}
                <br>
                <br>
            </div>
            @endif

                    <!-- Dynamic Table Full -->
            <div class="block">
                <div class="block-header">
                </div>
                <div class="block-content">
                    <table id='table_files' class="table compact" width="100%">
                        <thead>
                        <tr>
                            <!--
                            <th>  trans('filemanager.th_full_path') }} </th>
                            -->
                            <th> {{  trans('filemanager.th_name') }} </th>
                            <th> {{  trans('filemanager.th_mime') }} </th>
                            <th> {{  trans('filemanager.th_size') }} </th>
                            <th> {{  trans('filemanager.th_date') }} </th>
                            <th> {{  trans('filemanager.th_prio') }} </th>
                            <th data-sortable="false"> {{  trans('filemanager.th_actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        <!-- FOLDERS -->
                        @foreach ($data['subfolders_details'] as $subfolder)
                            <tr>
                                <!--
                                <td>
                                    <a href="filesystem.create?path=$subfolder['full_path'] }}" class='folder_link'>
                                        <i class='glyphicon glyphicon-folder-close'></i>
                                    </a>
                                </td>
                                -->
                                <td>

                                    <a href="filesystem.create?path={{$subfolder['full_path'] }}"
                                       class='folder_link'><img src="/images/folder-50.png"/>
                                        {{ $subfolder['name'] }}
                                    </a>

                                </td>
                                <td>{{ $subfolder['mime'] }}</td>
                                <td>-</td>
                                <td> {{ $subfolder['date'] }} </td>
                                <td>{{ $subfolder['priority'] }} </td>
                                <td>
                                    <a href="filesystem.delete_dir?path={{$data['full_path'] }}&name={{ $subfolder['name'] }}"
                                       class='folder_link'><i class='glyphicon glyphicon-remove'></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                                    <!-- FILES -->
                            @foreach ($data['files_details'] as $file)
                                <tr>
                                    <!--
                                    <td>
                                        <a href="filesystem.create?path=$file['full_path'] }}" class='folder_link'><i
                                                    class='glyphicon glyphicon-file'></i>
                                             $file['full_path'] }}
                                        </a>
                                    </td>
                                    -->

                                    @if ($file['mime'] == "pdf")

                                        <td>
                                            <a href="/web/viewer.html?file={{ $file['physic_path']}}">
                                                <img src="/images/pdf-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>
                                    @elseif($file['mime'] == "image")
                                        <td>
                                            <a href="{{ $file['physic_path'] }}" data-lightbox="image-1"
                                               class='folder_link'><img src="/images/image-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>
                                        <!-- <a href="#" class="preview_image" data-filename=" $file['name'] }}" data-full_path="$data['full_path'] }}" class='folder_link'><img src="/images/image-50.png"/>
                                        </a>
                                        -->
                                    @elseif($file['mime'] == "text")
                                        <td>
                                            <a href="filesystem.download?path={{$data['full_path'] }}&name={{ $file['name'] }}&mime={{ $file['mime'] }}"
                                               class='folder_link'><img src="/images/text-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>
                                    @elseif($file['mime'] == "video")
                                        <td>
                                            <a href="filesystem.play_video?path={{$data['full_path']}}&name={{ $file['name'] }}"
                                               class='folder_link'><img src="/images/movie-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>

                                    @elseif($file['mime'] == "audio")
                                        <td>
                                            <a href="filesystem.play_audio?path={{$data['full_path']}}&name={{ $file['name'] }}"
                                               class='folder_link'><img src="/images/music-50c.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>

                                    @elseif($file['mime'] == "excel")

                                        <td>
                                            <a href="https://docs.google.com/viewer?url=www.cloud.algoritex.com{{$file['physic_path']}}">
                                                <img src="/images/excel-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>

                                    @elseif($file['mime'] == "ppt")
                                        <td>
                                            <a href="https://docs.google.com/viewer?url=www.cloud.algoritex.com{{$file['physic_path']}}">
                                                <img src="/images/ppt-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>

                                    @elseif($file['mime'] == "word")
                                        <td>
                                            <a href="https://docs.google.com/viewer?url=www.cloud.algoritex.com{{$file['physic_path']}}">
                                                <img src="/images/word-50.png"/>
                                                {{ $file['name'] }}
                                            </a>

                                        </td>
                                    @else
                                        <td>
                                            <a href="filesystem.download?path={{$data['full_path'] }}&name={{ $file['name'] }}&mime={{ $file['mime'] }}"
                                               class='folder_link'><img src="/images/uknown-50.png"/>
                                                {{ $file['name'] }}
                                            </a>
                                        </td>

                                    @endif

                                    <td>{{ $file['mime'] }}</td>
                                    <td>{{ $file['size'] }}</td>
                                    <td>{{ $file['date'] }}</td>
                                    <td>{{ $file['priority'] }}</td>

                                    <td>
                                        @if ($file['mime'] == "pdf")
                                            <a href="/web/viewer.html?file={{ $file['physic_path']}}">
                                                <i class='glyphicon glyphicon-eye-open'></i>
                                            </a>
                                        @elseif ($file['mime'] == "image")
                                            <a href="{{ $file['physic_path'] }}" data-lightbox="image-1"
                                               class='folder_link'>
                                                <i class='glyphicon glyphicon-eye-open'></i>
                                            </a>
                                        @endif
                                        <a href="filesystem.download?path={{$data['full_path'] }}&name={{ $file['name'] }}&mime={{ $file['mime'] }}"
                                           class='folder_link'><i class='glyphicon glyphicon-cloud-download'></i>
                                        </a>
                                        @if ($user->role == "admin")
                                            <a href="filesystem.delete_file?path={{$data['full_path'] }}&name={{ $file['name'] }}"
                                               class='folder_link'><i class='glyphicon glyphicon-remove'></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Dynamic Table Full -->
    </div>
</div>


<!-- Modal User Details
       =============================================


<div class="modal fade" id="modal_video" tabindex="-1" role="dialog">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

                <video width="320" height="240" controls>
                    <source src="$file['physic_path']}}" type="video/mp4">
                    <source src="$file['physic_path']}}" type="video/ogg">
                </video>
        </div>
    </div>
</div>



<!-- Modal User Details
       =============================================

<div class="modal fade" id="modal_audio" tabindex="-1" role="dialog">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                <audio controls>
                    <source src="$file['physic_path']}}" type="audio/mpeg">
                </audio>
        </div>
    </div>
</div>
-->