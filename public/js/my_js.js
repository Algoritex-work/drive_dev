var physicPath = "/HD";

// Loading
/*
$(document).ajaxStart(function () {
    $("#modal_loader").modal("show");

});

$(document).ajaxStop(function () {
    $("#modal_loader").modal("hide");
});
*/

jQuery(document).ready(function () {

    $.ajax();

    createDatatable();

    function createDatatable() {
        folderDatatable = $('#table_files').DataTable({
            responsive: true,
            pagingType: "simple",
            "bInfo": false,
            "searching": false,
            "order": [[ 4, "desc" ]],
            "bLengthChange": false,
            "iDisplayLength": 50,
            "aoColumns": [
               //null,
                { "sClass": "put_center" },
                { "sClass": "put_center" },
                { "sClass": "put_center" },
                { "sClass": "put_center" },
                { "sClass": "put_center" },
                { "sClass": "put_center" }
            ],
            "columnDefs": [
                {
                    targets: [4],
                    visible: false
                },
                {
                    responsivePriority: 1, targets: 0
                },
                {
                    responsivePriority: 2, targets: -1
                }
            ],
            "language": {
                /*
                 "buttons": {
                 "pageLength": {
                 "_": "Mostrar %d"
                 }
                 },
                 */
                "lengthMenu": "Mostrar _MENU_",
                "emptyTable": "Carpeta vac&iacute;a",
                "infoEmpty": "Mostrando 0 de 0 de un total de 0 entradas",
                "loadingRecords": "Cargando...",
                "info": "Mostrando _START_ de _END_ con un total de _TOTAL_ entradas",
                "search": "Buscar:",
                "zeroRecords": "No coincide ninguna entrada",
                "paginate": {
                    "first": "Primero",
                    "last": "&Uacute;ltimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    }

    $(document).on('click', ".preview_image", function (event) {
        event.preventDefault();
        var name = $(this).attr("data-filename");
        var path = $(this).attr("data-full_path");

        var img_url = "";
        if (path == "./" || path == "ROOT") {
            img_url = physicPath + "/" + name;
        }
        else {
            path = path.replace("./", "");
            img_url = physicPath + "/" + path + "/" + name;
        }

        preview_image(img_url);
    });


    function preview_image(path) {
        $("#preview-image").attr("src", path);
        $("#modal-image-view").modal("show");
    }



    $(document).on('click', ".modal_audio", function (event) {
        event.preventDefault();

        var path = $(this).attr("data-full_path");
        var file = "";
        if (path == "./" || path == "ROOT") {
            file = physicPath + "/" + name;
        }
        else {
            path = path.replace("./", "");
            file = physicPath + "/" + path + "/" + name;
        }
        alert(path);

    });



    $(document).on('click', ".modal_video", function (event) {
        event.preventDefault();

        var path = $(this).attr("data-full_path");
        var file = "";
        if (path == "./" || path == "ROOT") {
            file = physicPath + "/" + name;
        }
        else {
            path = path.replace("./", "");
            file = physicPath + "/" + path + "/" + name;
        }
        alert(path);

    });




});