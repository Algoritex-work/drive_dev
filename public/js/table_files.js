
var folderDatatable;
var dataset;
var rootPath = './';
var currentPath = '';
var full_path = '';

jQuery(document).ready(function() {

    currentPath = rootPath;


    $.ajax({
        url: 'filesystem.create',
        type: 'POST',
       data: {
            'path': rootPath
        },
        dataType: "json"
    }).success(function (data){
        dataset = data["data_table_details"];
        bread_crum = data["breadcrum"];
        full_path = data["full_path"];
        generateBreadCrum(bread_crum);
        createDatatable(dataset);
        $("#form_upload").append("<input type='hidden' name='folder' value='" + full_path['full_path'] + "'>");
    });




    function generateData(data) {
        dataset = data["data_table_details"];
        bread_crum = data["breadcrum"];
        full_path = data["full_path"];
        generateBreadCrum(bread_crum);
        loadDataTable(dataset);
        $("#form_upload").append("<input type='hidden' name='folder' value='" + full_path['full_path'] + "'>");
    }


    function generateBreadCrum(bread_crum) {
        var breadcrumHTML = [];
        for ( var index = 0; index < bread_crum.length; index++) {
            var url = bread_crum[index]["full_path"];
            var name = bread_crum[index]["name"];
            breadcrumHTML.push("<li><a href=" + url + " class='breadcrum_link'>" + name + "</a></li>");
        }
        $('#route_path').empty();
        $('#route_path').append(breadcrumHTML);
    }



    $("#form_upload").submit(function(event){
        event.preventDefault();
        $.ajax({
            url: 'filesystem.create',
            type: 'POST',
            data: {
                'path': currentPath
            },
            dataType: "json"
        }).success(function (data) {
            $("#modal-file-upload").close();
            $("#modal-dialog").close();
            generateData(data);
        });
    });


    function createDatatable(dataset) {
        folderDatatable = jQuery('#table_files').DataTable({
            data: dataset,
            responsive: true,
            select: false,
            pagingType: "simple",
            "bInfo": false,
            "searching": false,
            "language": {
                "buttons": {
                    "pageLength": {
                        "_": "Mostrar %d"
                    }
                },
                "lengthMenu": "Mostrar _MENU_",
                "emptyTable":     "No hay datos disponibles",
                "infoEmpty":      "Mostrando 0 de 0 de un total de 0 entradas",
                "loadingRecords": "Cargando...",
                "info":           "Mostrando _START_ de _END_ con un total de _TOTAL_ entradas",
                /*"search":         "Buscar:",*/
                "zeroRecords":    "No coincide ninguna entrada",
                "paginate": {
                    "first":      "Primero",
                    "last":       "&Uacute;ltimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            },
            columns: [
                {
                    "data": "full_path"
                },
                {
                    "data": "name"
                },
                {
                    "data": "mime"
                },
                {
                    "data": "size"
                },
                {
                    "data": "date"
                },
                {
                    "data": "actions"
                }
            ],

            "columnDefs": [
                {
                    "targets": [0,2], visible: false,
                    /*
                    "data": "name",
                    "render": function (data) {
                        return "<a href='" + data + "' class='folder_link'>" + data + "</a>";
                    }
                    */
                },
                {
                    "targets": 1,
                    "render": function (data, type, row) {
                        if ( row['mime'] == "dir")
                            return "<a href='" + row['full_path'] + "' class='folder_link'><i class='glyphicon glyphicon-folder-close'> " + data + "</i></a>";
                        else
                            return "<a href='" + row['full_path'] + "' class='folder_link'><i class='glyphicon glyphicon-file'> " + data + "</i></a>";
                    }
                }
             ]
        });
    }

    function loadDataTable(dataset)
    {
        folderDatatable.clear();
        folderDatatable.rows.add(dataset);
        folderDatatable.columnDefs = [
            {
                /*
                "targets": 0,
                "data": "name",
                "render": function (data) {
                    return "<a href='" + currentPath + data + "' class='folder_link'>" + data + "</a>";
                }
                */
            },
            {
                "targets": 1,
                "data": "name",
                "render": function (data, type, row) {
                    if ( row['mime'] == "dir")
                        return "<a href='" + row['full_path'] + "' class='folder_link'><i class='glyphicon glyphicon-folder-close'> " + data + "</i></a>";
                    else
                        return "<a href='" + row['full_path'] + "' class='folder_link'><i class='glyphicon glyphicon-file'> " + data + "</i></a>";
                }
            }
        ]
        folderDatatable.draw();
    }


    $(document).on('click', ".breadcrum_link", function(event){
        //$('.breadcrum_link').click( function (event) {
        event.preventDefault();
        var path = $(this).attr("href");
        $.ajax({
            url: 'filesystem.create',
            type: 'POST',
            data: {
                'path': path
            },
            dataType: "json"
        }).success(function (data){
            generateData(data);
        });
    });


    $(document).on('click', ".folder_link", function(event){
        //$('.breadcrum_link').click( function (event) {
        event.preventDefault();
        var path = $(this).attr("href");
        $.ajax({
            url: 'filesystem.create',
            type: 'POST',
            data: {
                'path': path
            },
            dataType: "json"
        }).success(function (data){
            generateData(data);
        });
    });




});






