
//jQuery(document).ready(function() {

    jQuery('#table_users').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: -1 }
        ],
        select: false,
        pagingType: "simple",
        "iDisplayLength": 50,
        "language": {
            "lengthMenu": "Mostrar _MENU_",
            "emptyTable": "Carpeta vac&iacute;a",
            "infoEmpty": "Mostrando 0 de 0 de un total de 0 entradas",
            "loadingRecords": "Cargando ...",
            "info": "Mostrando _START_ de _END_ con un total de _TOTAL_ entradas",
            "search": "Buscar:",
            "zeroRecords": "No coincide ninguna entrada",
            "paginate": {
                "first": "Primero",
                "last": "&Uacute;ltimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
//});
