<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;

use Log;

use App\Libraries\StorageManager;
use Illuminate\Support\Facades\Auth;

class FileManagerController extends Controller
{
    private $path = '';
    private $manager = '';
    private $root = "";
    private $root_name = "";

    function __construct() {
        $this->manager = new StorageManager();
        $this->path = ".";
        $this->root = "./";
        $this->root_name = "ROOT";
    }


    function index() {
        try {
            $user =  Auth::user();
            $data = $this->getAllDataFromFolder($this->root);
            return view("main", ["data" => $data, "user" => $user]);
        }
        catch (\Exception $e) {
            Log::error("Failed index, " . $e->getMessage());
        }
    }


    function generateData(Request $request)
    {
        try {
            $this->path = $request->get('path');
            if ($this->path == ".")
                $this->path = "./";

            $data = $this->getAllDataFromFolder($this->path);
            $user =  Auth::user();
            return view("main", ["data" => $data, "user" => $user]);
        }
        catch (\Exception $e) {
            Log::error("Failed generate data, " . $e->getMessage());
        }

    }



    function getAllDataFromFolder($path) {

        try {
            $full_path = $this->manager->retrievePath($path);
            $folder_name = $this->manager->retrieveFolderName($path);
            $bread_crum = $this->manager->retrieveBreadCrum($path);
            $subfolders_details = $this->manager->retrieveFolders($path);
            $files_details =  $this->manager->retrieveFiles($path);
            $data = [
                "full_path" => $full_path,
                "folder_name" => $folder_name,
                "breadcrum" => $bread_crum,
                "subfolders_details" => $subfolders_details,
                "files_details" => $files_details,
            ];

            return $data;
        }
        catch (\Exception $e) {
            Log::error("Failed get all data from folder, " . $e->getMessage());
        }
    }



    public function uploadFile(Request $request)
    {
        try {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $this->path = $request->get('current_path'). "/" . $name;
            $result = $this->manager->saveFile($this->path , $file);

            if ($result === true) {
                return redirect()
                    ->back()
                    ->with(["message" => "Archivo  " . $name . " subido al drive correctamente"]);
            }

            return redirect()
                ->back()
                ->with(["error" => "Error, no se pudo completar la subida de " . $name]);
        }
        catch (\Exception $e) {
            Log::error("Failed upload file, " . $e->getMessage());
        }
    }


    function createFolder(Request $request) {

        try {
            $name = $request->input('new_folder');
            $this->path = $request->get('current_path'). "/" . $name;
            $result = $this->manager->addFolder($this->path);
            if ($result === true) {
                return redirect()
                    ->back()
                    ->with(["message" => "Carpeta " . $name . " creada"]);
            }

            return redirect()
                ->back()
                ->with(["error" => "Error, no se pudo crear la carpeta" . $name]);
        }
        catch (\Exception $e) {
            Log::error("Failed create folder, " . $e->getMessage());
        }
    }

    function deleteFolder(Request $request) {

        try {
            $name = $request->get('name');
            $this->path = $request->get('path');
            $result = $this->manager->removeFolder($this->path, $name);

            if ($result === true) {
                    return redirect()
                        ->back()
                        ->with(["message" => "Carpeta " . $name . " borrada."]);
            }

            return redirect()
                ->back()
                ->with(["error" => "Error, no se pudo borrar la carpeta " . $name]);
        }
        catch (\Exception $e) {
            Log::error("Failed delete folder, " . $e->getMessage());
        }
    }

    function deleteFile(Request $request) {
        try {
            $name = $request->get('name');
            $this->path = $request->get('path');
            $result = $this->manager->removeFile($this->path, $name);

            if ($result === true) {
                return redirect()
                    ->back()
                    ->with(["message" => "Archivo " . $name . " borrado."]);
            }

            return redirect()
                ->back()
                ->with(["error" => "Error, no se pudo borrar el archivo  " . $name]);
        }
        catch (\Exception $e) {
            Log::error("Failed delete file, " . $e->getMessage());
        }
    }


    public function getDownload(Request $request){
        try {
            $this->path = $request->get('path');
            $name = $request->get('name');
            $mime =  $request->get('mime');
            $headers = "Content-Type:" .$mime;

            return $this->manager->downloadFile($this->path, $name, urlencode($headers));
        }
        catch (\Exception $e) {
            Log::error("Failed get download, " . $e->getMessage());
        }
    }


    public function searchFile(Request $request) {

        try {
            $query = $request->get('search_query');
            $query = filter_var($query, FILTER_SANITIZE_STRING);
            $data = $this->manager->search($query);
            if ( isset($data)) {
                $user =  Auth::user();
                return view("main", ["data" => $data, "user" => $user]);
            }
            else {
                return redirect()
                    ->back()
                    ->with(["error" => "Archivo " .$query. " no encontrado"]);
            }
        }
        catch (\Exception $e) {
            Log::error("Failed search file, " . $e->getMessage());
        }
    }

    public function goMain(){
        return redirect("/")
        ->with(["message" => "Archivo borrado"]);
    }


    public function postImage(Request $request) {
        try {
            $this->path = $request->get('path');
            $name = $request->get('name');

            return $this->manager->getPhysicFile($this->path, $name);
        }
        catch (\Exception $e) {
            Log::error("Failed post image, " . $e->getMessage());
        }
    }


    public function playVideo(Request $request) {
        try {
            $path = $request->get('path');
            $name = $request->get('name');
            if ($path !== "./")
                $link = $path."/".$name;
            else
                $link = $path.$name;

            $link = str_replace("./", "/", $link);
            $link = "/HD".$link;
            return view("prev_audio_video.preview", ["link" => $link, "type" => "video"]);
        }
        catch (\Exception $e) {
            Log::error("Failed post image, " . $e->getMessage());
        }
    }

    public function playAudio(Request $request) {
        try {
            $path = $request->get('path');
            $name = $request->get('name');
            if ($path !== "./")
                $link = $path."/".$name;
            else
                $link = $path.$name;

            $link = str_replace("./", "/", $link);
            $link = "/HD".$link;
            return view("prev_audio_video.preview", ["link" => $link, "type" => "audio"]);
        }
        catch (\Exception $e) {
            Log::error("Failed post image, " . $e->getMessage());
        }
    }


}
