<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user = Auth::user();
            $all_users_database = User::all();
            return view('admin', ["data" => $all_users_database, "user" => $user]);
        } catch (\Exception $e) {
            Log::error("Failed index, " . $e->getMessage());
        }
    }

    public function updateRole(Request $request)
    {

        try {
            $role = filter_var($request->input('role'), FILTER_SANITIZE_STRING);
            $email = filter_var($request->input('email_user'), FILTER_SANITIZE_EMAIL);
            $user_to_update = User::findOrFail($email);
            $user_to_update->role = $role;
            $user_to_update->save();
            return redirect()
                ->back()
                ->with(["message" => "Usuario: " . $user_to_update->email . " " . $user_to_update->full_name . " ha cambiado de rol a " . $user_to_update->role]);
        } catch (\Exception $e) {
            Log::error("Failed update role, " . $e->getMessage());
            return redirect()
                ->back()
                ->with(["error" => "Error, No se pudo cambiar el rol para el usuario: " . $user_to_update->email . " " . $user_to_update->full_name]);
        }
    }
}