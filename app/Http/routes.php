<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('php_info', function () {
    echo phpinfo();
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'FileManagerController@index');
    Route::get('filesystem.create', 'FileManagerController@generateData');
    Route::post('filesystem.upload', 'FileManagerController@uploadFile');
    Route::post('filesystem.folder', 'FileManagerController@createFolder');
    Route::get('filesystem.download', 'FileManagerController@getDownload');
    Route::get('filesystem.delete_dir', 'FileManagerController@deleteFolder');
    Route::get('filesystem.delete_file', 'FileManagerController@deleteFile');
    Route::post('filesystem.image', 'FileManagerController@postImage');
    Route::post('filesystem.search', 'FileManagerController@searchFile');
    Route::get('filesystem.search', 'FileManagerController@goMain');
    Route::get('filesystem.play_video', 'FileManagerController@playVideo');
    Route::get('filesystem.play_audio', 'FileManagerController@playAudio');
    Route::get('admin', 'AdminController@index');
    Route::post('admin.update_role', 'AdminController@updateRole');


});

// AUTHENTICATION ROUTES PART
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
