<?php
namespace App\Libraries;

use Dflydev\ApacheMimeTypes\PhpRepository;
use Storage;
use Log;
use Carbon\Carbon;
use \League\Flysystem;
use Symfony\Component\Debug\Debug;


class StorageManager
{

    protected $disk = '';
    protected $mime = '';
    protected $physicHDPath = '';

    public function __construct()
    {

        $this->disk = Storage::disk('local');
        $this->mime = new PhpRepository();
        $this->root_path = "ROOT";
        $this->physicHDPath = env('HD_PATH');
    }


    function retrieveBreadCrum($path)
    {
        try {
            $breadcrum = array();
            if ($path == "./" || $path == ".") {
                $breadcrum [] = ["full_path" => "./", "name" => $this->root_path];
            } else {
                $folders = explode("/", $path);
                $partial_path = "";
                foreach ($folders as $folder) {
                    if ($folder == ".") {
                        $partial_path .= $folder;
                        $folder = $this->root_path;
                    } else {
                        $partial_path .= "/" . $folder;
                    }

                    $crum = ["full_path" => $partial_path, "name" => $folder];
                    $breadcrum [] = $crum;
                }
            }
            return $breadcrum;
        } catch (\Exception $e) {
            Log::error("Failed retrieve breadcrum, " . $e->getMessage());
        }
    }


    function retrieveFolders($path)
    {
        try {
            if ($path == ".")
                $path = "./";
            $folders = $this->disk->directories($path);
            $items = array();
            $current_path = $path;
            foreach ($folders as $folder) {
                $folder = basename($folder);
                if ($current_path == "./")
                    $full_path = $current_path . $folder;
                else
                    $full_path = $current_path . "/" . $folder;

                $data = $this->getPathDetails($full_path, $folder);
                $items[] = $data;
            }
            return $items;
        } catch (\Exception $e) {
            Log::error("Failed retrieve folders, " . $e->getMessage());
        }
    }


    function retrieveFiles($path)
    {
        try {

            $items = array();
            $files = $this->disk->files($path);
            $current_path = $path;
            foreach ($files as $file) {
                $file = basename($file);
                if ($current_path == "./")
                    $full_path = $current_path . $file;
                else
                    $full_path = $current_path . "/" . $file;

                $data = $this->getFileDetails($full_path, $file);
                $items[] = $data;
            }
            return $items;
        } catch (\Exception $e) {
            Log::error("Failed retrieve files, " . $e->getMessage());
        }
    }


    function retrieveFolderName($path)
    {
        if ($path == "." || $path == "./")
            return $this->root_path;
        else
            return basename($path);
    }


    function retrievePath($path)
    {
        if ($path == ".")
            $path = "./";
        return $path;
    }


    function getTypeFile($mime)
    {

        if (isset ($mime)) {
            if (strpos($mime, "pdf"))
                $mime = "pdf";
            else if (strpos($mime, "jpeg") || strpos($mime, "png") || strpos($mime, "jpg") || strpos($mime, "JPG") || strpos($mime, "JEPG"))
                $mime = "image";
            else if (strpos($mime, "text"))
                $mime = "text";
            else if (strpos($mime, "mpeg"))
                $mime = "audio";
            else if (strpos($mime, "mp4"))
                $mime = "video";
            else if (strpos($mime, "spreadsheet"))
                $mime = "excel";
            else if (strpos($mime, "presentation"))
                $mime = "ppt";
            else if (strpos($mime, "word"))
                $mime = "word";
            else if (strpos($mime, "plain"))
                $mime = "text";
            else
                $mime = "desconocido";
            return $mime;
        }
        $mime = "desconocido";
        return $mime;
    }


    function getPathDetails($full_path, $name)
    {
        try {
            $mime = $this->mime->findType(pathinfo($full_path, PATHINFO_EXTENSION));

            if (!isset ($mime)) {
                $mime = "directorio";
                $size = "-";
            } else {
                $mime = $this->getTypeFile($mime);
                $bytes = $this->disk->size('/' . $full_path);
                $size = $this->human_filesize($bytes);
            }

            $timestamp = $this->disk->lastModified($full_path);
            $date = Carbon::createFromTimestamp($timestamp);
            $date = $date->format("Y-m-d");
            $details = [
                "full_path" => trim($full_path),
                "physic_path" => str_replace("./", "/HD/", $full_path),
                "name" => trim($name),
                "mime" => trim($mime),
                "size" => trim($size),
                "date" => $date,
                "priority" => "1",
            ];
            return $details;
        } catch (\Exception $e) {
            Log::error("Failed get path details, " . $e->getMessage());
        }
    }


    function getFileDetails($full_path, $name)
    {
        try {
            $mime = $this->mime->findType(pathinfo($full_path, PATHINFO_EXTENSION));


            $mime = $this->getTypeFile($mime);
            $bytes = $this->disk->size('/' . $full_path);
            $size = $this->human_filesize($bytes);


            $timestamp = $this->disk->lastModified($full_path);
            $date = Carbon::createFromTimestamp($timestamp);
            $date = $date->format("Y-m-d");
            $details = [
                "full_path" => trim($full_path),
                "physic_path" => str_replace("./", "/HD/", $full_path),
                "name" => trim($name),
                "mime" => trim($mime),
                "size" => trim($size),
                "date" => $date,
                "priority" => "0",
            ];
            return $details;
        } catch (\Exception $e) {
            Log::error("Failed get file details, " . $e->getMessage());
        }
    }

    /*
    function getFileDetails($full_path, $name)
    {
        try {
            if (!isset ($mime)) {
                $mime = "directorio";
                $size = "-";
            } else {
                $mime = $this->getTypeFile($mime);
                $bytes = $this->disk->size('/' . $full_path);
                $size = $this->human_filesize($bytes);
            }

            $temp_path = $full_path . "/" . $name;
            echo " full_path: ". $full_path;
            $mime = $this->mime->findType(pathinfo($temp_path, PATHINFO_EXTENSION));

            $mime = $this->getTypeFile($mime);
            echo "  mime: ". $mime;

            $bytes = $this->disk->size($temp_path);
            echo "  bytes: ". $bytes;
            $size = $this->human_filesize($bytes);
            echo "  size: ". $size;
            $timestamp = $this->disk->lastModified($temp_path);
            $date = Carbon::createFromTimestamp($timestamp);
            $date = $date->format("Y-m-d");
            echo "  date: ". $date;
            echo "<br>";

            $details = [
                "full_path" => trim($full_path),
                "physic_path" => str_replace("./", "/HD/", $temp_path),
                "name" => trim($name),
                "mime" => trim($mime),
                "size" => trim($size),
                "date" => $date,
            ];

            return $details;
        }
        catch(\Exception $e){
            Log::error("Failed get file details, " . $e->getMessage());
        }
    }

    */

    public function search($query)
    {
        try {
            foreach ($this->disk->allFiles("./") as $file) {

                $name = strstr(basename($file), '.', true);
                if (str_contains($query, $name)) {

                    $full_path = "./" . $file;

                    $items[] = $this->getFileDetails($full_path, basename($file));
                    $folder_name = $this->retrieveFolderName(dirname($file));

                    if (dirname($file) == ".")
                        $bread_crum = $this->retrieveBreadCrum("./");
                    else
                        $bread_crum = $this->retrieveBreadCrum("./" . dirname($file));

                    $subfolders_details = [];

                    return [
                        "full_path" => "./" . dirname($file),
                        "folder_name" => $folder_name,
                        "breadcrum" => $bread_crum,
                        "subfolders_details" => $subfolders_details,
                        "files_details" => $items,
                    ];
                }
            }
            return null;
        } catch (\Exception $e) {
            Log::error("Failed search, " . $e->getMessage());
        }
        return null;
    }


    public function saveFile($path, $content)
    {
        try {
            if ($this->disk->exists($path)) {
                return "File already exists.";
            }

            return $this->disk->put($path, $content);
        } catch (\Exception $e) {
            Log::error("Failed save file, " . $e->getMessage());
        }
    }

    public function addFolder($path)
    {
        try {
            if ($this->disk->exists($path)) {
                return "Path already exists.";
            }
            return $this->disk->makeDirectory($path);
        } catch (\Exception $e) {
            Log::error("Failed add folder, " . $e->getMessage());
        }
    }

    function removeFolder($path, $name)
    {
        try {
            $path .= "/" . $name;

            return $this->disk->deleteDirectory($path);
        } catch (\Exception $e) {
            Log::error("Failed remove folder, " . $e->getMessage());
        }
    }

    public function removeFile($path, $name)
    {
        try {
            if ($path !== "./")
                $path .= "/";

            return $this->disk->delete($path . $name);
        } catch (\Exception $e) {
            Log::error("Failed remove file, " . $e->getMessage());
        }
    }

    public function downloadFile($path, $name, $headers)
    {
        try {
            $path = str_replace("./", "/", $path);
            if ($path !== "/")
                $path .= "/";
            $path = $this->physicHDPath . $path . $name;

            return response()->download($path);
        } catch (\Exception $e) {
            Log::error("Failed download file, " . $e->getMessage());
        }
    }

    function printPath($items)
    {
        foreach ($items as $item) {
            echo "name: " . $item['name'] . " Type: " . $item['mime'];
            echo " size: " . $item['size'];
            echo " date: " . $item['date'] . "<br><br><br>";
        }
    }


    function human_filesize($bytes, $decimals = 2)
    {
        $size = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
        @$size[$factor];
    }

    /*
    public function getPhysicFile($path, $name) {


        $path = str_replace("./", "/", $path);
        if ($path !== "/")
            $path .= "/";
        $path =  $this->physicHDPath.$path.$name;


        if ($path == "./")
            $path = $path.$name;
        else
            $path = $path . "/" .$name;


        //$file = $this->disk->get($path);
        //return response()->make($file, 200);
        // configure with favored image driver (gd by default)
        //$file = Image::make(public_path(). "/images/img.jpg");

        //var_dump($file);
        //$mime = $this->mime->findType(pathinfo($path, PATHINFO_EXTENSION));
        //var_dump($path);
        //$path = public_path(). "/images/img.jpg";
        //var_dump($path);
	    //return response()->make(file_get_contents($path), 304);
        //return $file->response();
        //var_dump($file);
        //var_dump($mime);

        //var_dump($path);
        //return $image->response()->response('jpg'); //will ensure a jpg is always returned
    }
    */

}